package br.com.paulo.phonecell.servlets;

import br.com.paulo.phonecell.model.EstoqueCelular;
import br.com.paulo.phonecell.model.rest.EncapsulaCelulares;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by paulo on 07/04/16.
 */

@WebServlet("/celular/*")
public class ServletCelular extends HttpServlet{
    private EstoqueCelular estoqueCelular = new EstoqueCelular();
    private static JAXBContext contexto;

    static {
        try {
            contexto = JAXBContext.newInstance(EncapsulaCelulares.class);
        } catch (JAXBException jaxbeException) {
            throw new RuntimeException(jaxbeException);
        }
    }

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        String cabecalhoRecebido = request.getHeader("accept");

        if (cabecalhoRecebido == null || cabecalhoRecebido.contains("application/xml")) {
            escreveXML(request, response);
        }
        else if (cabecalhoRecebido.contains("application/json")){
            escreveJSON(request, response);
        }
        else{
            //o formato nao é suportado.
            response.sendError(415);
        }
    }

    /**
     * Trata objetos mapeados em xml e converte-os em xml.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void escreveXML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        EncapsulaCelulares encapsulaCelulares = new EncapsulaCelulares();

        //popula o objeto com os dados retornados dentro do construtor que está no set.
        encapsulaCelulares.setCelularesEncapsulados(new ArrayList<>(estoqueCelular.listarCelular()));

        PrintWriter responseWriter = response.getWriter();

        try {
            response.setContentType("application/xml;charset=UTF-8");
            Marshaller marshaller = contexto.createMarshaller();
            marshaller.marshal(encapsulaCelulares, responseWriter);
        }

        catch (Exception e) {

            //erro no content type
            response.sendError(500, e.getMessage());
        }
    }

    private void escreveJSON(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        EncapsulaCelulares encapsulaCelulares= new EncapsulaCelulares();
        encapsulaCelulares.setCelularesEncapsulados(new ArrayList<>(estoqueCelular.listarCelular()));
        try {
            response.setContentType("application/json;charset=UTF-8");

            MappedNamespaceConvention mappedNamespaceConvention = new MappedNamespaceConvention();
            XMLStreamWriter xmlStreamWriter = new MappedXMLStreamWriter(mappedNamespaceConvention, response.getWriter());

            Marshaller serializar = contexto.createMarshaller();
            serializar.marshal(encapsulaCelulares, xmlStreamWriter);
        } catch (JAXBException e) {
            response.sendError(500);
        }
    }

    /**
     * Obtem o identificador para realizar uma pesquisa especifica por um recurso.
     * @param request
     * @return
     * @throws RecursoSemIdentificadorException
     */
    private String obtemIdentificador(HttpServletRequest request) throws RecursoSemIdentificadorException {
        String uriRequisicao = request.getRequestURI();
        String[] pedacosDaUri = uriRequisicao.split("/");
        boolean achouContextoCelular = false;

        for (String contextoPedacos : pedacosDaUri) {
            if (contextoPedacos.equals("celular")) {
                achouContextoCelular = true;
                continue;
            }

            //Se achar o contexto na uri, o mesmo será decodificado
            if (achouContextoCelular) {
                try {
                    return URLDecoder.decode(contextoPedacos, "UTF-8");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        throw new RecursoSemIdentificadorException ("Recurso sem identificador");
    }

    private Object localizaObjetoParaEnvio(HttpServletRequest request){
        Object object =  null;

        try {
            object = estoqueCelular.recuperarCelularPeloNome(identificador);
        }
        catch (RecursoSemIdentificadorException e){
            e.printStackTrace();
        }
    }
}
