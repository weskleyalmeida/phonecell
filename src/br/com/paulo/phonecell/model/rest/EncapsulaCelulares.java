package br.com.paulo.phonecell.model.rest;

import br.com.paulo.phonecell.model.Celular;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by paulo on 07/04/16.
 */


@XmlRootElement //define como elemento root de uma estrutura de xml
public class EncapsulaCelulares {
    private Collection<Celular> celularesEncapsulados = new ArrayList<>();

    @XmlElement (name = "celular") //define como um dos elementos da estrutura em xml e mapeia cada um dos objetos da coleção
    public Collection<Celular> getCelularesEncapsulados() {
        return celularesEncapsulados;
    }

    public void setCelularesEncapsulados(Collection<Celular> celularesEncapsulados) {
        this.celularesEncapsulados = celularesEncapsulados;
    }
}
